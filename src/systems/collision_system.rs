use crate::play_audio::{play_bounce_sound, Sounds};
use crate::Jumper::{Collider, Rigidbody2D};
use amethyst::{
    assets::AssetStorage,
    audio::{output::Output, Source},
    core::math::Vector3,
    core::transform::Transform,
    derive::SystemDesc,
    ecs::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage},
};

#[derive(SystemDesc)]
pub struct CollisionSystem;

impl<'s> System<'s> for CollisionSystem {
    type SystemData = (
        ReadStorage<'s, Collider>,
        ReadStorage<'s, Transform>,
        WriteStorage<'s, Rigidbody2D>,
        Read<'s, AssetStorage<Source>>,
        ReadExpect<'s, Sounds>,
        Option<Read<'s, Output>>,
    );

    fn run(
        &mut self,
        (colliders, transforms, mut rbs, storage, sounds, audio_out): Self::SystemData,
    ) {
        for (t, rb) in (&transforms, &mut rbs).join() {
            for (t2, c) in (&transforms, &colliders).join() {
                //This looping structure is pretty flawed.
                //We loop over every collider for every rigidbody.
                //if the rigidbody is static, we continue.
                //if not, we check for collision
                //if so, we make the rigidbody move up.
                if rb.is_static {
                    continue;
                }

                if wrapped_pt_in_rect(t.translation(), t2.translation(), [c.w, c.h], rb.size) {
                    //TODO: Fiddle with this more to stop super-jumping

                    //play sound
                    play_bounce_sound(&*sounds, &storage, audio_out.as_deref());

                    if rb.vel[1] > 1.5 {
                        rb.vel[1] = 0.5;
                    } else {
                        rb.vel[1] += 1.0;
                    }
                }
            }
        }
    }
}

fn point_in_rect(x: f32, y: f32, left: f32, bottom: f32, right: f32, top: f32) -> bool {
    x >= left && x <= right && y >= bottom && y <= top
}

pub fn wrapped_pt_in_rect(
    t: &Vector3<f32>,
    t2: &Vector3<f32>,
    col: [f32; 2],
    rb: [f32; 2],
) -> bool {
    let rb_x = t.x;
    let rb_y = t.y;
    let col_x = t2.x - (col[0] * 0.5);
    let col_y = t2.y - (col[1] * 0.5);

    let rb_x_rad = rb[0] / 2.0;
    let rb_y_rad = rb[1] / 2.0;

    point_in_rect(
        rb_x,
        rb_y,
        col_x - rb_x_rad,
        col_y - rb_y_rad,
        col_x + col[0] + rb_x_rad,
        col_y + col[1] + rb_y_rad,
    )
}
