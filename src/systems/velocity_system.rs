use crate::Jumper::Rigidbody2D;
use amethyst::{
    core::transform::Transform,
    derive::SystemDesc,
    ecs::{Join, ReadStorage, System, SystemData, WriteStorage},
};

#[derive(SystemDesc)]
pub struct VelocitySystem;

impl<'s> System<'s> for VelocitySystem {
    type SystemData = (ReadStorage<'s, Rigidbody2D>, WriteStorage<'s, Transform>);

    fn run(&mut self, (rbs, mut transforms): Self::SystemData) {
        for (rb, trans) in (&rbs, &mut transforms).join() {
            if rb.is_static {
                continue;
            }

            let v = rb.vel;

            trans.set_translation_x(trans.translation().x + v[0]);
            trans.set_translation_y(trans.translation().y + v[1]);
        }
    }
}
