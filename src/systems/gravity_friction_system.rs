use crate::Jumper::Rigidbody2D;
use amethyst::{
    core::transform::Transform,
    derive::SystemDesc,
    ecs::{Join, ReadStorage, System, SystemData, WriteStorage},
};

#[derive(SystemDesc)]
pub struct GravityFrictionSystem;

impl<'s> System<'s> for GravityFrictionSystem {
    type SystemData = (WriteStorage<'s, Rigidbody2D>, ReadStorage<'s, Transform>);

    fn run(&mut self, (mut rbs, transforms): Self::SystemData) {
        const GRAVITY: f32 = 1.5;
        const FRICTION: f32 = 1.5;
        const TERMINAL_VEL: f32 = 2.5;

        for (rb, _trasns) in (&mut rbs, &transforms).join() {
            if rb.vel[1].abs() < 0.01 {
                rb.vel[1] = 0.0;
            }
            if rb.vel[0].abs() < 0.01 {
                rb.vel[0] = 0.0;
            }
            //if we don't have these ifs, the velocity gets stupidly low (like 20dp)

            if rb.vel[1] > 0.0 {
                rb.vel[1] /= GRAVITY;
            } else if rb.vel[1] < 0.0 {
                rb.vel[1] *= GRAVITY;
            } else {
                rb.vel[1] -= GRAVITY / 2.0;
            }

            rb.vel[0] /= FRICTION; //X

            if rb.vel[1] <= -TERMINAL_VEL {
                rb.vel[1] = -TERMINAL_VEL;
            }
        }
    }
}
