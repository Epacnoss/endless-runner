use crate::systems::collision_system::wrapped_pt_in_rect;
use crate::Jumper::{Collider, ARENA_HEIGHT, ARENA_WIDTH};
use amethyst::core::math::Vector3;
use amethyst::{
    core::{transform::Transform, Time},
    derive::SystemDesc,
    ecs::{Join, Read, ReadStorage, System, SystemData, WriteStorage},
    renderer::Camera,
};
use rand::Rng;

#[derive(SystemDesc)]
pub struct MoveCameraSystem;

impl<'s> System<'s> for MoveCameraSystem {
    type SystemData = (
        ReadStorage<'s, Camera>,
        ReadStorage<'s, Collider>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>,
    );

    fn run(&mut self, (cams, colliders, mut transforms, time): Self::SystemData) {
        // let tim = time.absolute_time_seconds() as i32;
        for (c, t) in (&cams, &mut transforms).join() {
            //Just get the transforms of the camera(s)
            // t.set_translation_y(t.translation().y + (tim as f32)/2.0);
            t.set_translation_y(t.translation().y + 0.1);
        }
    }
}
