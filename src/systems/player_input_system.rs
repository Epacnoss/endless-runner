use crate::systems::collision_system::wrapped_pt_in_rect;
use crate::Jumper::{Collider, Rigidbody2D};
use amethyst::{
    core::{SystemDesc, Transform},
    derive::SystemDesc,
    ecs::{Join, Read, ReadStorage, System, SystemData, WriteStorage},
    input::{InputHandler, StringBindings},
};

#[derive(SystemDesc)]
pub struct PlayerInputSystem;

impl<'s> System<'s> for PlayerInputSystem {
    // The same BindingTypes from the InputBundle needs to be inside the InputHandler
    type SystemData = (
        Read<'s, InputHandler<StringBindings>>,
        WriteStorage<'s, Rigidbody2D>,
        ReadStorage<'s, Transform>,
        ReadStorage<'s, Collider>,
    );

    fn run(&mut self, (input, mut rigidbodies, transforms, colls): Self::SystemData) {
        const GROUND_CHECK_ALLOWANCE: f32 = 50.0;
        const GROUND_CHECK_RAD: f32 = GROUND_CHECK_ALLOWANCE / 2.0;

        for (mut rb, trans) in (&mut rigidbodies, &transforms).join() {
            if rb.is_player {
                let horiz = input.axis_value("horizontal").unwrap_or(0.0);
                rb.vel[0] += horiz / 2.5;

                if input.action_is_down("jump").unwrap_or(false) {
                    //loop over colliders to check
                    let mut works = false;
                    let mut t1 = trans.translation().clone();
                    t1.x -= GROUND_CHECK_ALLOWANCE;

                    for (trans2, col) in (&transforms, &colls).join() {
                        if wrapped_pt_in_rect(
                            &t1,
                            trans2.translation(),
                            [col.w, col.h],
                            [GROUND_CHECK_RAD; 2],
                        ) {
                            works = true;
                        }
                    }

                    if works {
                        rb.vel[1] += 2.5;
                    }
                }
            }
        }
    }
}
