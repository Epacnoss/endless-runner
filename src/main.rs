mod Jumper;
mod play_audio;
mod systems;

use amethyst::audio::AudioBundle;
use amethyst::{
    core::TransformBundle,
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    ui::RenderUi,
    utils::application_root_dir,
};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;
    let display_conf_path = app_root.join("config").join("display.ron");

    let binding_path = app_root.join("config").join("bindings.ron");
    let input_bundle =
        InputBundle::<StringBindings>::new().with_bindings_from_file(binding_path)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_conf_path)?
                        .with_clear([0.00196, 0.23726, 0.21765, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
                .with_plugin(RenderUi::default()),
        )?
        .with_bundle(TransformBundle::new())?
        .with_bundle(input_bundle)?
        .with_bundle(AudioBundle::default())?
        .with(systems::VelocitySystem, "velocity_system", &[])
        .with(
            systems::CollisionSystem,
            "collision_system",
            &["velocity_system", "audio_system"],
        )
        .with(
            systems::GravityFrictionSystem,
            "friction_system",
            &["velocity_system"],
        )
        .with(
            systems::PlayerInputSystem,
            "player_input_system",
            &["input_system", "velocity_system"],
        )
        .with(systems::MoveCameraSystem, "move_camera_system", &[]);

    let assets_dir = app_root.join("assets");
    let mut game = Application::new(assets_dir, Jumper::Jumper::default(), game_data)?;
    game.run();

    Ok(())
}
