use amethyst::assets::AssetStorage;
use amethyst::{
    assets::Loader,
    audio::{output::Output, Source, SourceHandle, WavFormat},
    ecs::{World, WorldExt},
};

const BOUNCE_SOUND: &str = "sound/collision.wav";

pub struct Sounds {
    pub bounce_sfx: SourceHandle,
}

fn load_track(loader: &Loader, world: &World, file: &str) -> SourceHandle {
    loader.load(file, WavFormat, (), &world.read_resource())
}

pub fn init_audio(world: &mut World) {
    let sfx = {
        let loader = world.read_resource::<Loader>();
        Sounds {
            bounce_sfx: load_track(&loader, &world, BOUNCE_SOUND),
        }
    };
    world.insert(sfx);
}

pub fn play_bounce_sound(sounds: &Sounds, storage: &AssetStorage<Source>, output: Option<&Output>) {
    if let Some(ref output) = output.as_ref() {
        if let Some(sound) = storage.get(&sounds.bounce_sfx) {
            output.play_once(sound, 1.0);
        }
    }
}
