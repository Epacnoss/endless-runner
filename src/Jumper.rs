use crate::play_audio::init_audio;
use amethyst::core::ecs::NullStorage;
use amethyst::ui::UiImage;
use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::transform::Transform,
    ecs::{Component, DenseVecStorage},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
};
use rand::Rng;

pub const ARENA_HEIGHT: f32 = 100.0;
pub const ARENA_WIDTH: f32 = 100.0;

fn init_camera(world: &mut World) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(ARENA_WIDTH * 0.5, ARENA_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(ARENA_WIDTH, ARENA_HEIGHT))
        .with(trans)
        // .with(CamFlag::default())
        .build();
}

pub struct Collider {
    pub w: f32,
    pub h: f32,
}
impl Component for Collider {
    type Storage = DenseVecStorage<Self>;
}
impl Collider {
    fn from_default_platform() -> Self {
        Collider {
            w: PLATFORM_WIDTH,
            h: PLATFORM_HEIGHT,
        }
    }
}

pub const PLATFORM_HEIGHT: f32 = 3.0;
pub const PLATFORM_WIDTH: f32 = 11.0;

fn init_platforms(world: &mut World, sprite_sheet_handle: Handle<SpriteSheet>, gap: f32) {
    let spr = SpriteRender::new(sprite_sheet_handle, 0);
    let mut current_row = 0;

    for i in 0..30 {
        for j in 0..25 {
            if rand::thread_rng().gen_range(0..15) > 1 {
                continue;
            }

            let mut trans = Transform::default();

            let y: f32 = (current_row as f32) * 10.0;

            let x: f32 = (j as f32) * 5.0;

            trans.set_translation_xyz(x, y, 0.0);

            world
                .create_entity()
                .with(spr.clone())
                .with(Collider::from_default_platform())
                .with(Rigidbody2D::new_wh(
                    true,
                    [PLATFORM_WIDTH, PLATFORM_HEIGHT],
                    false,
                ))
                .with(trans)
                .build();
        }
        current_row += 1;
    }
}

fn init_player(world: &mut World, handle: Handle<SpriteSheet>) {
    let mut trans = Transform::default();
    trans.set_translation_xyz(50.0, 50.0, 0.0);
    let spr = SpriteRender::new(handle, 1);

    world
        .create_entity()
        .with(spr)
        .with(Rigidbody2D::new_rad(false, 3.0, true))
        .with(trans)
        .build();
}

pub struct Rigidbody2D {
    pub vel: [f32; 2],
    pub size: [f32; 2],
    pub is_static: bool,
    pub is_player: bool,
}
impl Component for Rigidbody2D {
    type Storage = DenseVecStorage<Self>;
}
impl Rigidbody2D {
    pub fn new_wh(is_static: bool, size: [f32; 2], player: bool) -> Self {
        Rigidbody2D {
            vel: [0.0, 0.0],
            size,
            is_static,
            is_player: player,
        }
    }
    pub fn new_rad(is_static: bool, rad: f32, player: bool) -> Self {
        Rigidbody2D {
            vel: [0.0, 0.0],
            size: [rad; 2],
            is_static,
            is_player: player,
        }
    }
}

fn load_tex<N>(name: N, world: &World) -> Handle<Texture>
where
    N: Into<String>,
{
    let loader = world.read_resource::<Loader>();
    loader.load(
        name,
        ImageFormat::default(),
        (),
        &world.read_resource::<AssetStorage<Texture>>(),
    )
}

#[derive(Default)]
pub struct Jumper {
    platform_gap: Option<f32>,
    spirte_sheet_handle: Option<Handle<SpriteSheet>>,
}

impl SimpleState for Jumper {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let mut world = data.world;

        self.platform_gap.replace(5.0);

        {
            let tex = load_tex("texture/spritesheet.png", world);
            let loader = world.read_resource::<Loader>();
            self.spirte_sheet_handle.replace(loader.load(
                "texture/spritesheet.ron",
                SpriteSheetFormat(tex),
                (),
                &world.read_resource::<AssetStorage<SpriteSheet>>(),
            ));
        }

        world.register::<UiImage>();

        init_platforms(world, self.spirte_sheet_handle.clone().unwrap(), 5.0);
        init_player(world, self.spirte_sheet_handle.clone().unwrap());
        init_camera(world);
        init_audio(world);
    }
    // fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
    //     //do stuff
    //
    //     Trans::None
    // }
}
